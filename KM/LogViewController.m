#import "LogViewController.h"
#import "AppDelegate.h"

@implementation LogViewController

#pragma mark -
#pragma mark UIViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self synchronize];
}

#pragma mark -
#pragma mark LogViewController

- (void)synchronize
{
    self.log.text = [[NSUserDefaults standardUserDefaults] stringForKey:
            kLogKey];
}

- (void)showOverlay
{
    self.overlay.alpha = .7;
    
    [self performSelector:@selector(hideOverlay) withObject:nil afterDelay:5.];
}

- (void)hideOverlay
{
    self.overlay.alpha = 0.;
}
@end
