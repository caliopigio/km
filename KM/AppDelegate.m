#import "AppDelegate.h"
#import "AppDelegate+CoreData.h"
#import "MapViewController.h"
#import "LogViewController.h"
#import "OdometerViewController.h"
#import "Location.h"

NSString *const kLogKey = @"log_key";
NSString *const kLastLocationKey = @"last_location_key";
NSString *const kDistanceKey = @"distance_key";
NSString *const kInitialDistanceKey = @"initial_distance_key";
NSString *const kLocationsKey = @"locations_key";
NSString *const kDistanceNotification = @"distance_notification";

@implementation AppDelegate
{
    CLLocationManager *_locationManager;
    LogViewController *_log;
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#pragma mark -
#pragma mark AppDelegate

- (void)showLocalNotification
{
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = @"LOCATION FIX";
    notification.hasAction = NO;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:
            notification];
}

- (void)makeKeyWindow
{
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    _log = [[LogViewController alloc] init];
    _log.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Log" image:[UIImage
            imageNamed:@"log"] tag:100];
    MapViewController *map = [[MapViewController alloc] init];
    map.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Map" image:[UIImage
            imageNamed:@"map"] tag:101];
    UITabBarController *tabBar = [[UITabBarController alloc] init];
    OdometerViewController *odo = [[OdometerViewController alloc] init];
    odo.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Odometer" image:nil
            tag:102];
    tabBar.viewControllers = [NSArray arrayWithObjects:odo, map, _log, nil];
    
    //[_log synchronize];
    self.window.rootViewController = tabBar;
    
    [self.window makeKeyAndVisible];
}

- (void)log:(NSString *)log
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *lastLocation = [userDefaults dictionaryForKey:
            kLastLocationKey];
    NSMutableString *completeLog = [NSMutableString stringWithFormat:
            @"%@\nDistance: %.3f Km\nTime: %.6f h\nSpeed: %.1f Km/h\nTotal "\
                "Distance: %.1f Km\n\n",
                log,
                [[lastLocation objectForKey:@"distance"] floatValue],
                [[lastLocation objectForKey:@"time"] floatValue],
                [[lastLocation objectForKey:@"speed"] floatValue],
                [userDefaults floatForKey:kDistanceKey]];
    id logObject = [userDefaults objectForKey:kLogKey];

    if (logObject) {
        [completeLog appendString:(NSString *)logObject];
    }
    [userDefaults setObject:completeLog forKey:kLogKey];
    [userDefaults synchronize];
    [_log synchronize];
}

#pragma mark -
#pragma mark <UIApplicationDelegate>

- (BOOL)          application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    
    [_locationManager startMonitoringSignificantLocationChanges];
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]) {
        //[self showLocalNotification];
    } else {
        [self makeKeyWindow];
    }
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if (!self.window) {
        [self makeKeyWindow];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self saveContext];
}

#pragma mark -
#pragma mark <CLLocationManagerDelegate>

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations lastObject];
    
    if ([[currentLocation timestamp] timeIntervalSinceNow] < -5.) {
        [self log:@"LOCATION FIX DISCARDED"];
        return;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *lastLocation = [defaults dictionaryForKey:kLastLocationKey];
    float distance = [currentLocation distanceFromLocation:[[CLLocation alloc]
            initWithLatitude:[[lastLocation objectForKey:@"latitude"]
                doubleValue] longitude:[[lastLocation objectForKey:@"longitude"]
                doubleValue]]] / 1000;
    float time = [currentLocation.timestamp timeIntervalSinceDate:
            [lastLocation objectForKey:@"timestamp"]] / 3600;
    float speed = distance / time;
    
    if ((speed >= 10.) && (speed <= 280.)) {
        float newDistance = [defaults floatForKey:kDistanceKey] + distance +
                [defaults floatForKey:kInitialDistanceKey];
        
        [defaults setFloat:newDistance forKey:kDistanceKey];
        [defaults setFloat:.0 forKey:kInitialDistanceKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:
                kDistanceNotification object:self];
    } else if (speed < 10.) {
        [defaults setFloat:distance forKey:kInitialDistanceKey];
    }
    NSDictionary *currentLocationAsDictionary = @{
        @"latitude" :
            [NSNumber numberWithDouble:currentLocation.coordinate.latitude],
        @"longitude" :
            [NSNumber numberWithDouble:currentLocation.coordinate.longitude],
        @"timestamp" :
            currentLocation.timestamp,
        @"distance" :
            [NSNumber numberWithDouble:distance],
        @"time" :
            [NSNumber numberWithDouble:time],
        @"speed":
            [NSNumber numberWithDouble:speed],
    };
    [defaults setObject:currentLocationAsDictionary forKey:kLastLocationKey];
    [defaults synchronize];
    
    Location *newLocation = [NSEntityDescription
            insertNewObjectForEntityForName:@"Location" inManagedObjectContext:
                [self managedObjectContext]];
    newLocation.latitude = [NSNumber numberWithDouble:
            currentLocation.coordinate.latitude];
    newLocation.longitude = [NSNumber numberWithDouble:
            currentLocation.coordinate.longitude];
    newLocation.timestamp = currentLocation.timestamp;
    newLocation.distance = [NSNumber numberWithDouble:distance];
    newLocation.time = [NSNumber numberWithDouble:time];
    newLocation.speed = [NSNumber numberWithDouble:speed];
    
    [self saveContext];
    switch ([[UIApplication sharedApplication] applicationState]) {
        case UIApplicationStateActive:
            [_log showOverlay];
            break;
        default:
            //[self showLocalNotification];
            break;
    }
    [self log:@"LOCATION FIX"];
}
@end
