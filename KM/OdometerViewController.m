#import "OdometerViewController.h"
#import "AppDelegate.h"

@implementation OdometerViewController

#pragma mark -
#pragma mark UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector
            (update:) name:kDistanceNotification object:nil];
    [self update:nil];
}

#pragma mark -
#pragma mark OdometerViewController

- (void)update:(id)sender
{
    float distance = [[NSUserDefaults standardUserDefaults] floatForKey:
            kDistanceKey];
    float current = [self.last.text floatValue] + distance;
    self.current.text = [NSString stringWithFormat:@"%.1f", current];
    self.progress.progress = distance / ([self.next.text floatValue] -
            [self.last.text floatValue]);
}
@end
