#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

extern NSString *const kLogKey;
extern NSString *const kLastLocationKey;
extern NSString *const kDistanceKey;
extern NSString *const kInitialDistanceKey;
extern NSString *const kLocationsKey;
extern NSString *const kDistanceNotification;

@interface AppDelegate : UIResponder <UIApplicationDelegate,
        CLLocationManagerDelegate>
{
    NSManagedObjectContext *_managedObjectContext;
    NSManagedObjectModel *_managedObjectModel;
    NSPersistentStoreCoordinator *_persistentStoreCoordinator;
}

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext
        *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel
        *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator
        *persistentStoreCoordinator;
@end
