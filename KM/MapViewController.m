#import "MapViewController.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "Location.h"

@implementation MapViewController
{
    NSArray *_locations;
    MKPointAnnotation *_annotation;
    NSInteger index;
}

#pragma mark -
#pragma mark UIViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication
            sharedApplication] delegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:
            @"Location"];
    NSError *error;
    _locations = [context executeFetchRequest:request error:
            &error];
    index = _locations.count - 1;
    
    [self checkButtons];
    [self setNewLocation];
    [self.map addAnnotation:_annotation];
}

#pragma mark -
#pragma mark MapViewController

- (IBAction)buttonDidTap:(UIButton *)button
{
    if ((button.tag == 100) && (index > 0)) {
        index --;
    } else if ((button.tag) == 101 && (index < _locations.count - 1)) {
        index ++;
    }
    if ((index >= _locations.count) || (index < 0)) {
        button.enabled = NO;
    } else {
        [self setNewLocation];
    }
    [self checkButtons];
}

- (void)setNewLocation
{
    Location *location = [_locations objectAtIndex:index];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(
            [[location latitude] doubleValue],
                [[location longitude] doubleValue]);
    
    if (!_annotation) {
        _annotation = [[MKPointAnnotation alloc] init];
    }
    _annotation.coordinate = coordinate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"d MMM h:mm:ss a"];
    
    _annotation.title = [formatter stringFromDate:location.timestamp];
    _annotation.subtitle = [NSString stringWithFormat:@"Speed: %.1f Km/h",
            [[location speed] doubleValue]];
    self.map.region = MKCoordinateRegionMakeWithDistance(_annotation.coordinate,
            5000, 5000.);
}

- (void)checkButtons
{
    if (index > 0) {
        [(UIControl *)[self.view viewWithTag:100] setEnabled:YES];
    } else {
        [(UIControl *)[self.view viewWithTag:100] setEnabled:NO];
    }
    if (index < _locations.count - 1) {
        [(UIControl *)[self.view viewWithTag:101] setEnabled:YES];
    } else {
        [(UIControl *)[self.view viewWithTag:101] setEnabled:NO];
    }
}
@end
