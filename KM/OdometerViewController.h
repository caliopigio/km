#import <UIKit/UIKit.h>

@interface OdometerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *last;
@property (weak, nonatomic) IBOutlet UILabel *next;
@property (weak, nonatomic) IBOutlet UILabel *current;
@property (weak, nonatomic) IBOutlet UIProgressView *progress;
@end
