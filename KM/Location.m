#import "Location.h"


@implementation Location

@dynamic distance;
@dynamic latitude;
@dynamic longitude;
@dynamic timestamp;
@dynamic time;
@dynamic speed;

@end
