#import <UIKit/UIKit.h>

@interface LogViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *log;
@property (weak, nonatomic) IBOutlet UIView *overlay;

- (void)synchronize;
- (void)showOverlay;
@end
