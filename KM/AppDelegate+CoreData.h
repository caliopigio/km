#import "AppDelegate.h"

@interface AppDelegate (CoreData)

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@end
