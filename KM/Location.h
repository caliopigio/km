#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSNumber *distance;
@property (nonatomic, retain) NSNumber *latitude;
@property (nonatomic, retain) NSNumber *longitude;
@property (nonatomic, retain) NSDate *timestamp;
@property (nonatomic, retain) NSNumber *time;
@property (nonatomic, retain) NSNumber *speed;

@end
